# README

## Intro

This [PICs](https://en.wikipedia.org/wiki/PIC_microcontroller) software is an example
of what will be used on the PICs of the Tenkou satellite. It serves **development
purposes only**, i.e. will not be uploaded onto any of the flight PICs.
The intended PIC microcontroller is PIC16F877-04/PQ by Mirochip.

## How do I get set up?

* You will need the following prerequisites:
    * [TenkouGPIO library](https://bitbucket.org/AleksanderLidtke/tenkougpio)
	* [TenkouSPIMaster library](https://bitbucket.org/rafarodleon/tenkouspimaster)
	* [TenkouSPISlave library (if you want to connect two PICs together)](https://bitbucket.org/rafarodleon/tenkouspislave)
    * TenkouUART library (if you want to use UART for testing)
* The library is tested with the free version of the XC8 compiler.

The MPLAB projects that are maintained in the repository assume the above repositories will be cloned to the following folders located in one directory:

`./tenkougpio/`

`./TenkouSPIMaster/`

`./TenkouSPISlave/`

## Contact

If in doubt, contact [Alek Lidtke](mailto:aleksander.lidtke@gmail.com) or someone from the team
on [Slack](kitokuyamalab.slack.com).