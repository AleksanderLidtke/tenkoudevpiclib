/* Ten-Koh TK-01-01-01 OBC PCB header file, which defines all the pins of the two
 * included PICs: MCU (master control unit) and BCU (backup control unit).
 * Intended for use with PIC16F877 and version of the PCB newer than 1.1.6.
 */
#ifndef TK_01_01_01
#define	TK_01_01_01

/*
 *******************************************************************************
 *  Pin definitions.
 *******************************************************************************
 */

// SPI Master pins and initialisation registers.
#define SPI_MOSI_MASTER RB3 // OUT = MOSI - master out slave in. Data to slave
#define SPI_MISO_MASTER RB1 // IN = MISO - master in slave out. Data from slave
#define SPI_CK_MASTER   RB2 // OUT = SCK - SPI clock
#define SPI_MOSI_MASTER_INIT TRISB3
#define SPI_MISO_MASTER_INIT TRISB1
#define SPI_CK_MASTER_INIT TRISB2

// SPI chip selects for other subsystems.
#define SPI_BUS_CS_EPS RA5 // Both EPS PICs in TK-02-05-01.
#define SPI_BUS_CS_COMM1 RA4 // Comms. line 1 in TK-03-04-01.
#define SPI_BUS_CS_COMM2 RA3 // Comms. line 2 in TK-03-04-01.
#define SPI_BUS_CS_EPS_INIT TRISA5
#define SPI_BUS_CS_COMM1_INIT TRISA4
#define SPI_BUS_CS_COMM2_INIT TRISA3

// Interrupts routed to the OBC.
#define GPIO_OBC_EPS RB0 // From TK-02 EPS.
#define GPIO_OBC_COMM1 RB4 // From one line of telecommunications in TK-03. 
#define GPIO_OBC_COMM2 RB5 // From the other line of telecommunications in TK-03.
#define GPIO_OBC_EPS_INIT TRISB0
#define GPIO_OBC_COMM1_INIT TRISB4
#define GPIO_OBC_COMM2_INIT TRISB5

// MCU-BCU interface.
#define MCU_BCU_RESET RD4 // Used by the MCU to reset the BCU and vice-versa.
#define UART_RX RC7 // Rx on MCU, Tx on BCU.
#define UART_TX RC6 // Vice-versa, Tx on MCU.
#define MCU_BCU_RESET_INIT TRISD4
#define UART_RX_INIT TRISC7
#define UART_TX_INIT TRISC6

// System-wide I2C, using the in-built PIC module.
#define I2C_SCL RC3
#define I2C_SDL RC4
#define I2C_SCL_INIT TRISC3
#define I2C_SDL_INIT TRISC4

// Misc. internal OBC pins.
#define RTC_I2C_ENABLE RD0 // Pull high to enable RTC level shifter.
#define SD_SPI_ENABLE RD1 // Pull high to enable SD card level shifter.
// CS for the OBC SD card is actually defined in the
// SD sd_communication_information_t struct.
#define SPI_BUS_CS_SD RD2
// CS for the OBC temperature measurement ADC is actually defined in the
// adc_communication_information_t struct.
#define SPI_BUS_CS_ADC RD3
#define RTC_I2C_ENABLE_INIT TRISD0
#define SD_SPI_ENABLE_INIT TRISD1
#define SPI_BUS_CS_SD_INIT TRISD2
#define SPI_BUS_CS_ADC_INIT TRISD3

#endif	/* XC_HEADER_TEMPLATE_H */

