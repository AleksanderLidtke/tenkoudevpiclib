#include "spi_master.h"
#include "spi_slave.h"
#include "TenkouGPIO.h"
#include "TK-01-01-01_OBC.h" // OBC pin definitions.
#include <stdint.h> //To use uint_16, etc.
#include <xc.h>
#include <stdlib.h>

#include "uart.h"
#include "i2c_master.h"
#include "sd.h"
#include "adc.h"

#define UART_BAUD 9600 // Bits per second.
// Function declarations.

