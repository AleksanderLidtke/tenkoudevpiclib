/*
 * File:   main.c
 * Author: alek
 *
 * Created on 01 August 2017, 09:37
 */
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

#define _XTAL_FREQ     20000000

#include <xc.h>
#include "TenkouDevPIC.h"

unsigned char uartTerminator[] = {"XXX\n"}; // Distinct end of debug messages.
unsigned char rtcWriteAddr = 0xD0; // 7-bit (1101000) RTC address plus write 0 at the end.
unsigned char rtcReadAddr = 0xD1; // RTC address plus read 1 at the end.
unsigned char rtcSecsAddr = 0x00; // RTC seconds register address.
    
// Convert between RTC date format and binary (numbers) back and forth.
uint8_t bcd2bin(uint8_t val){return val - 6 * (val >> 4);}
uint8_t bin2bcd(uint8_t val){return val + 6 * (val / 10);}

int dummyFunction(void)
/* Does nothing. */
{
    return 0;
}

int blinkUnused(void)
/* Blink unused pins RE0, RE1 and RE2. Once. */
{
    // Set as outputs.
    TRISE0 = 0; TRISE1 = 0; TRISE2 = 0;
    ADCON1=0x0F; // Set RE<0:2>, RA1 and RA5 as digital pins.
    // Blink once.
    RE0=0; RE1=0; RE2=0;
    __delay_ms(500);
    RE0=1; RE1=1; RE2=1;
    __delay_ms(500);
    
    return 0;
}

int blinkSPIPins(void)
/* Blink all SPI master pins and CEes. Don't touch SD_SPI_ENABLE. */
{
    // Test individual SPI pins.
    SPI_MOSI_MASTER=1;
    SPI_MISO_MASTER=1;
    SPI_CK_MASTER=1;
    SPI_BUS_CS_SD=1;
    SPI_BUS_CS_ADC=1;
    __delay_ms(500);
    SPI_MOSI_MASTER=0;
    SPI_MISO_MASTER=0;
    SPI_CK_MASTER=0;
    SPI_BUS_CS_SD=0;
    SPI_BUS_CS_ADC=0;
}

int blinkSubsystemCSes(void)
/* Blink subsystem chip selects in the header. Once. */
{
    // Set as outputs.
    SPI_BUS_CS_EPS_INIT=0;
    SPI_BUS_CS_COMM1_INIT=0;
    SPI_BUS_CS_COMM2_INIT=0;
    
    ADCON1=0x06; // Set RE<0:2>, RA0, RA1, RA2, RA3 and RA5 as digital pins.
    
    // Blink once.
    SPI_BUS_CS_EPS=0; SPI_BUS_CS_COMM1=0; SPI_BUS_CS_COMM2=0;
    __delay_ms(500);
    SPI_BUS_CS_EPS=1; SPI_BUS_CS_COMM1=1; SPI_BUS_CS_COMM2=1;
    __delay_ms(500);
    
    return 0;
}

int blinkSubsystemInterruptPins(void)
/* Blink subsystem interrupt pins in the header. Once. */
{
    // Set as outputs.
    GPIO_OBC_EPS_INIT = 0; GPIO_OBC_COMM1_INIT = 0; GPIO_OBC_COMM2_INIT = 0;
    
    // Blink once.
    GPIO_OBC_EPS=0; GPIO_OBC_COMM1=0; GPIO_OBC_COMM2=0;
    __delay_ms(500);
    GPIO_OBC_EPS=1; GPIO_OBC_COMM1=1; GPIO_OBC_COMM2=1;
    __delay_ms(500);
    
    return 0;
}

int blinkI2CPins(void)
/* Blink the I2C pins once. */
{
    I2C_SCL_INIT = 0; I2C_SDL_INIT = 0; // Set as outputs in this function.
    I2C_SCL=1; I2C_SDL=1;
    __delay_ms(500);
    I2C_SCL=0; I2C_SDL=0;
    __delay_ms(500);
}

int test_sd_communication(void)
/* Record known data in the SD card, read it and transmit on UART to check
 * if they make sense and the SD card works. */
{
    SPI_MOSI_MASTER_INIT = 0;
    SPI_MISO_MASTER_INIT = 1;
    SPI_CK_MASTER_INIT = 0;
    SPI_BUS_CS_SD_INIT = 0;
    _delay(5000); // 1 ms for power on
    _delay(500); // 0.1 ms for power ramp up
    _delay(5000); // 1 ms for Initialization delay
    
    uint8_t tx_data_1[4] = {'A','B','C','D'};
    uint8_t tx_data_2[4] = {'E','F','G','H'};
    uint8_t tx_data_3[4] = {'I','J','K','L'};
    uint8_t tx_data_4[4] = {'M','N','O','P'};
    uint8_t tx_data_5[4] = {'Q','R','S','T'};
    
    uint8_t rx_data_1[4] = {0x00};
    uint8_t rx_data_2[4] = {0x00};
    uint8_t rx_data_3[4] = {0x00};
    uint8_t rx_data_4[4] = {0x00};
    uint8_t rx_data_5[4] = {0x00};
    
    // Define where the SD car CS is.
    sd_communication_information_t sd_condition;
        sd_condition.port = &PORTD;
        sd_condition.pin = 2;
        sd_condition.data_size = 4;
    
    sd_setup_SPImode(sd_condition);
    
    // Write known data to the card.
    for(uint32_t address = 1 ; address < 6 ; address++){
        sd_condition.address_number = address;
        switch(address){
            case 1:
                Write_SDcard(sd_condition,tx_data_1); break;
            case 2:
                Write_SDcard(sd_condition,tx_data_2); break;
            case 3:
                Write_SDcard(sd_condition,tx_data_3); break;
            case 4:
                Write_SDcard(sd_condition,tx_data_4); break;
            case 5:
                Write_SDcard(sd_condition,tx_data_5); break;
        }
    }
    
    // Read the previously sent data.
    for(uint32_t address = 1 ; address < 6 ; address++){
        sd_condition.address_number = address;
        switch(address){
            case 1:
                Read_SDcard(sd_condition,rx_data_1);
            case 2:
                Read_SDcard(sd_condition,rx_data_2);
            case 3:
                Read_SDcard(sd_condition,rx_data_3);
            case 4:
                Read_SDcard(sd_condition,rx_data_4);
            case 5:
                Read_SDcard(sd_condition,rx_data_5);
        }
    }
    // Transmit the read data over UART to see if they make sense.
    unsigned char sdMsg[]={"SD:"}; // Make the SD data stand out.
    uartWriteBytes(sdMsg,3);
    uartWriteBytes(rx_data_1,4);
    uartWriteBytes(uartTerminator,1); // Only show one separator between readings.
    uartWriteBytes(rx_data_2,4);
    uartWriteBytes(uartTerminator,1);
    uartWriteBytes(rx_data_3,4);
    uartWriteBytes(uartTerminator,1);
    uartWriteBytes(rx_data_4,4);
    uartWriteBytes(uartTerminator,1);
    uartWriteBytes(rx_data_5,4);
    uartWriteBytes(uartTerminator,1);
    
    return 0;
}

int test_adc_communication(void)
/* Read the output of the OBC ADC and send it over UART to see if it's OK. */
{
    SPI_MOSI_MASTER_INIT = 0;
    SPI_MISO_MASTER_INIT = 1;
    SPI_CK_MASTER_INIT = 0;
    SPI_BUS_CS_ADC_INIT = 0;
    
    uint16_t data[8] = {0}; // Raw conversion results from the ADC.
    float result[8] = {0}; // To save result of conversion_voltage.
    
    // Defines the CS for the ADC and its measurement settings.
    adc_communication_information_t trans_data;
        trans_data.port = &PORTD;
        trans_data.pin = 3;
        trans_data.range_select = DOUBLE_RANGE;
        trans_data.outcoding_select = STRAIGHT_BINARY;
    
    // Read all ADC channels.
    for(unsigned char i=0; i<8; i++){
        trans_data.channel_select = i;
        data[i] = adc_communication_normal(trans_data);
        result[i] = adc_conversion_voltage(data[i],DOUBLE_RANGE);
    }
    
    // Send the conversion results over UART for verification.
    unsigned char adcMsg[]={"ADC:"}; // Make the RTC data stand out.
    uartWriteBytes(adcMsg,4);
    for(unsigned char j = 0; j<8; j++){
        int status; // Float to char array cast status.
        char* buf; // float will be cast into this.
        buf = ftoa(result[j],&status);
        uartWriteBytes(buf,8); // float is 24 or 32 bits, depending on compiler flags.
        uartWriteBytes(uartTerminator,1); // Only show one separator between readings.
    }
    return 0;
}

int testTemperatureMeasurement(void)
/* Read the temperatures of the MCU and the BCU, convert ADC data to actual T
 * readings, and send them over UART to see they are OK. */
{
    SPI_MOSI_MASTER_INIT = 0;
    SPI_MISO_MASTER_INIT = 1;
    SPI_CK_MASTER_INIT = 0;
    SPI_BUS_CS_ADC_INIT = 0;
    
    uint16_t data[2]={0}; // Raw conversion results from the ADC.
    float temperatures[2]={0}; // Actual temperatures of MCU and BCU in C.
    
    // Defines the CS for the ADC and its measurement settings.
    adc_communication_information_t trans_data;
        trans_data.port=&PORTD;
        trans_data.pin=3;
        trans_data.range_select=DOUBLE_RANGE;
        trans_data.outcoding_select=STRAIGHT_BINARY;
    
    // Read the ADC - MCU T (IC10 on the PCB) on channel 1, BCU T (IC12) on channel 2.
    for(unsigned char i=0; i<2; i++){
        trans_data.channel_select=i;
        data[i]=adc_communication_normal(trans_data);
        temperatures[i]=adc_conversion_voltage(data[i],DOUBLE_RANGE); // ADC reading in V.
        // Scale temperatures[i] to degrees C. We use 6.8k resistance in series
        // with the transducer and the nominal output is 1 uA/K. The current at
        // 25 C is 298.2 uA i.e. 273.2 uA at 0 C.
        temperatures[i]=temperatures[i]/6800.*1000000.-273.2; // I=U/R, T=I/(1uA/K)
    }
    
    // Send the temperatures over UART for verification.
    unsigned char tMsg[]={"TMB:"}; // Make the T readings stand out.
    uartWriteBytes(tMsg,4);
    for(unsigned char j=0; j<2; j++){
        int status; // Float to char array cast status.
        char* buf; // float will be cast into this.
        buf=ftoa(temperatures[j],&status);
        uartWriteBytes(buf,10);
        uartWriteBytes(uartTerminator,1); // Only show one separator between readings.
    }
    return 0;
}

int readRTC(unsigned char* dateTime)
/* Get the current time from the RTC and record the read date and time in a vector
 * with the following entries: second, minute, month, day, month, year.
 * Return 0 if the read was successful.
 */
{
    i2cMasterInit(100000); // 100 kHz I2C frequency supported by the RTC.
    // Set the register pointer to a known location.
    i2cMasterStart(); // Issue the I2C start condition.
    i2cMasterWrite(&rtcWriteAddr); // Select the RTC by writing to it.
    i2cMasterWrite(&rtcSecsAddr); // Write to set the register pointer.
    i2cMasterStop();
    
    // Read time bytes. The DS1340 will keep outputting bytes from consecutive register
    // accesses until the register pointer reaches 07h. Then, the register pointer
    // will wrap around to 00h.
    i2cMasterStart(); // Issue the start condition.
    i2cMasterWrite(&rtcReadAddr); // Address byte = first byte after start condition.
    i2cMasterRead(0,dateTime); // Seconds.
    dateTime[0]=dateTime[0] & 0x7F; //TODO I don't get what &0x0F is for but they do it on the Internet.
    i2cMasterRead(0,dateTime+1); // Minutes.
    i2cMasterRead(0,dateTime+2); // Hours.
    i2cMasterRead(0,dateTime+3); // Skip day of week register.
    i2cMasterRead(0,dateTime+3); // Date, overwrite day of week.
    i2cMasterRead(0,dateTime+4); // Month.
    i2cMasterRead(1,dateTime+5); // Year. Send NACK to terminate the read.
    i2cMasterStop(); // Stops I2C communication.
    return 0;
    //TODO This seems to only read seconds correctly. It overwrites other dateTime
    // components with 16 (verified dateTime is printed to UART correctly if readRTC
    // isn't called). In binary, 16 is 0b10000 so maybe there's something wrong
    // with the bit shifting somewhere.
}

int printTimeToUART(unsigned char* dateTime)
/* Print the dateTime array with seconds, minutes, hours, day, month, year
 * to UART separating with the 1st byte of uartTerminator. UART has to be
 * initialised before calling this function. Returns 0 if the operation was
 * successful.*/
{
    unsigned char rtcMsg[]={"RTC:"}; // Make the RTC data stand out.
    uartWriteBytes(rtcMsg,4);
    for(unsigned char i=0;i<6;i++)
    {
        uint8_t temp=bcd2bin((uint8_t) dateTime[i]);
        if(temp<10) // Time component is only one digit.
        {
            char buf[1]; // Will cast into this. Need buffer for two digits.
            utoa(buf,temp,10);
            uartWriteBytes(buf,1); // Send this time component.
        }
        else // Need a larger buffer.
        {
            char buf[2];
            utoa(buf,temp,10);
            uartWriteBytes(buf,2);
        }
        uartWriteBytes(uartTerminator,1); // Only show one separator between readings.
    }
    return 0;
}

void main(void)
{
    // Initialise all output pins of the PIC. This has to be done in the main loop, not a function.
    SPI_MOSI_MASTER_INIT = 0; SPI_MISO_MASTER_INIT = 0; SPI_CK_MASTER_INIT = 0;
    //TODO One of the commented out pins makes the MCU_BCU_RESET function change state when other functions are called?
    //SPI_BUS_CS_EPS_INIT = 0; SPI_BUS_CS_COMM1_INIT = 0; SPI_BUS_CS_COMM2_INIT = 0;
    //GPIO_OBC_EPS_INIT = 0; GPIO_OBC_COMM1_INIT = 0; GPIO_OBC_COMM2_INIT = 0;
    //UART_RX_INIT = 0; UART_TX_INIT = 0; // These we haven't tried yet.
    //I2C_SCL_INIT=1; I2C_SDL_INIT=1;
    RTC_I2C_ENABLE_INIT = 0;
    SD_SPI_ENABLE_INIT = 0;
    SPI_BUS_CS_SD_INIT = 0; SPI_BUS_CS_ADC_INIT = 0;
    
    // Only turn both PICs on.
    MCU_BCU_RESET_INIT = 0;
    MCU_BCU_RESET = 0;
    
    // Check default states.
    int ret = blinkUnused(); // Blink RE0, RE1 and RE2 once.
    int dummy = dummyFunction();
        
    // Test mutual rebooting by rebooting the other PIC after some time.
/*    
    __delay_ms(1100); // Vary this between MCU and BCU so that they don't do this simultaneously.
    int ret = blinkUnused(); // See some more blinking until the end of the main loop.
    MCU_BCU_RESET = 0; // Short reset.
    __delay_ms(1000);
    MCU_BCU_RESET = 1;
    __delay_ms(20000);  // This has to be long for MCU so it's still in the main
                        //loop before the BCU reboots it after it was rebooted itself.
*/
    
    // Test SPI pins.
/*    SD_SPI_ENABLE=1; // Keep the level shifter on all the time.
    int ret = blinkSPIPins(); // Blink all SPI pins.
    int ret = blinkSubsystemCSes();
    
    // Test interrupt pins.
    int ret = blinkSubsystemInterruptPins();
*/
    // Test UART comms. by sending a string and marking beginning of the main loop.
    uartInit(UART_BAUD); // Initialise UART @ 9600 baud.
    unsigned char txbuffer1[] = {"Begin main loop."};         
    uartWriteBytes(txbuffer1,16);
    
    // Test the I2C level shifter.
/*
    RTC_I2C_ENABLE=1; // Level shifter on.
    int ret = blinkI2CPins();
    RTC_I2C_ENABLE=0; // Level shifter off.
    int ret = blinkI2CPins();
*/
    
    // Test I2C comms. by reading the RTC.
    unsigned char rdata[6]={1,2,3,4,5,6}; // For received data, year, month, day, second, minutes, hours.
    RTC_I2C_ENABLE=1; // Only communicate with the RTC w/o the level shifter.
    int ret = readRTC(rdata); // Get a time vector from the RTC.
    int ret = printTimeToUART(rdata); // Print the time to UART.
    __delay_ms(500); // Wait for the RTC to change the time it's showing.
    
    // Test the SD card functionality.
    SPI_BUS_CS_ADC=1; // Set high to make sure it isn't on the SPI bus.
    SD_SPI_ENABLE=1; // Need this on for the card to be accessible.
    __delay_us(1); // Wait tEN before using the level shifter.
    // This will (I think) overflow the UART buffer, so to test the SD and other
    // things need to comment one of the other tests out.
    //int ret = test_sd_communication();
    SD_SPI_ENABLE=0; // Done with the SD card.
    __delay_us(1); // Wait tDIS to turn off the level shifter.

    // Test OBC temperature ADC functionality by applying known voltage to R25 and R26.
    int ret = test_adc_communication();
    
    // Test if the values of the temperature transducers make sense.
    int ret = testTemperatureMeasurement(); // Will print T to UART.

    // When all tests are run and messages printed to UART, show it.
    uartWriteBytes(uartTerminator,4); // Distinct end of the data message.
}